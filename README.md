# biogame

Game for AP Biology project about cellular respiration. Written in Arcade

This game can either be run locally with python, as the precompiled executables seem to be broken.<!-- or by running the provided precompiled executables for your OS.--> 

**UNIX:** To run the game locally on UNIX operating systems, first run `pip3 install -U -r requirements.txt --no-binary=Pillow` in the directory where you cloned this project, and then run `python3 biogame.py`, substituting `pip3` and `python3` for your system's python and pip commands. 

**Windows:** Windows users will probably have more luck running it in [WSL](https://www.microsoft.com/en-us/p/ubuntu/9nblggh4msv6?activetab=pivot:overviewtab) and just following the UNIX install instructions, but if you want to run it natively, you will have to build Pillow from source, found [here](https://github.com/python-pillow/Pillow), after installing CMake 3.12 or higher and the Microsoft Visual Studio 2017 or newer with C++ component. You can find more instructions [here](https://github.com/python-pillow/Pillow/blob/master/winbuild/build.rst). After building Pillow, you just have to run `py -m pip install arcade` and then `py biogame.py`

<!--**Ubuntu**: [Download](https://gitlab.com/TabulateJarl8/biogame/-/jobs/artifacts/master/raw/dist/biogame?job=ubuntubuild)

**Arch Linux**: [Download](https://gitlab.com/TabulateJarl8/biogame/-/jobs/artifacts/master/raw/dist/biogame?job=archbuild)-->
