import arcade
import sys
import os
import time

SCREEN_WIDTH = 1000
SCREEN_HEIGHT = 650
SCREEN_TITLE = "Cell Respiration Game"

CHARACTER_SCALING = 1
TILE_SCALING = 0.5
COIN_SCALING = 0.5
SPRITE_PIXEL_SIZE = 128
GRID_PIXEL_SIZE = (SPRITE_PIXEL_SIZE * TILE_SCALING)
PLAYER_MOVEMENT_SPEED = 5
GRAVITY = 1
PLAYER_JUMP_SPEED = 20
UPDATES_PER_FRAME = 5

RIGHT_FACING = 0
LEFT_FACING = 1

# How many pixels to keep as a minimum margin between the character
# and the edge of the screen.
LEFT_VIEWPORT_MARGIN = 250
RIGHT_VIEWPORT_MARGIN = 250
BOTTOM_VIEWPORT_MARGIN = 50
TOP_VIEWPORT_MARGIN = 100

PLAYER_START_X = 64
PLAYER_START_Y = 225

def resource_path(relative_path):
	if os.path.isfile(relative_path):
		try:
			base_path = sys._MEIPASS
		except Exception:
			base_path = os.path.abspath(".")
		return os.path.join(base_path, relative_path)
	else:
		relative_path = os.path.split(relative_path)[1]
		try:
			base_path = sys._MEIPASS
		except Exception:
			base_path = os.path.abspath(".")
		return os.path.join(base_path, relative_path)

def load_texture_pair(filename):
	return [
		arcade.load_texture(resource_path(filename)),
		arcade.load_texture(resource_path(filename), flipped_horizontally=True)
	]

class PlayerCharacter(arcade.Sprite):
	def __init__(self):
		super().__init__()
		self.character_face_direction = RIGHT_FACING
		self.cur_texture = 0
		self.scale = CHARACTER_SCALING
		self.points = [[-22, -64], [22, -64], [22, 28], [-22, 28]]

		main_path = "assets/images/animated_characters/female_adventurer/femaleAdventurer"

		self.idle_texture_pair = load_texture_pair(f"{main_path}_idle.png")
		self.fall_texture_pair = load_texture_pair(f"{main_path}_fall.png")
		self.jump_texture_pair = load_texture_pair(f"{main_path}_jump.png")
		self.walk_textures = []
		for i in range(8):
			texture = load_texture_pair(f"{main_path}_walk{i}.png")
			self.walk_textures.append(texture)

	def update_animation(self, delta_time: float = 1/60):
		if self.change_x < 0 and self.character_face_direction == RIGHT_FACING:
			self.character_face_direction = LEFT_FACING
		elif self.change_x > 0 and self.character_face_direction == LEFT_FACING:
			self.character_face_direction = RIGHT_FACING

		if self.change_x == 0 and self.change_y == 0:
			self.texture = self.idle_texture_pair[self.character_face_direction]
			return
		elif self.change_y > 0:
			self.texture = self.jump_texture_pair[self.character_face_direction]
			return
		elif self.change_y < 0:
			self.texture = self.fall_texture_pair[self.character_face_direction]
			return

		self.cur_texture += 1
		if self.cur_texture > 7 * UPDATES_PER_FRAME:
			self.cur_texture = 0
		frame = self.cur_texture // UPDATES_PER_FRAME
		direction = self.character_face_direction
		self.texture = self.walk_textures[frame][direction]

class BioGame(arcade.Window):
	def __init__(self):
		super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
		self.player_list = None
		self.wall_list = None
		self.atp_list = None
		self.glucose_list = None
		self.o2_list = None
		self.background_list = None
		self.lever_list = None
		self.conveyer_belt_list = None
		self.onconveyer = 0

		self.player = None
		self.physics_engine = None
		self.view_bottom = 0
		self.view_left = 0
		self.atp = 0
		self.o2 = 0
		self.glucose = 0
		self.level = 1
		self.tick = 0
		self.trigger1 = 0

		self.collect_coin_sound = arcade.load_sound(resource_path("assets/sounds/coin1.ogg"))
		self.jump_sound = arcade.load_sound(resource_path("assets/sounds/jump1.ogg"))
		self.game_over = arcade.load_sound(resource_path("assets/sounds/gameover1.ogg"))
		self.lever_sound = arcade.load_sound(resource_path("assets/sounds/lever.ogg"))
		self.engine_start = arcade.load_sound(resource_path("assets/sounds/engine.ogg"))
		self.engine_continue = arcade.load_sound(resource_path("assets/sounds/engine_continue.ogg"))
		self.engine_stop = arcade.load_sound(resource_path("assets/sounds/engine_stop.ogg"))
		self.audioplayer = None
		self.glycolysis_sound = arcade.load_sound(resource_path("assets/sounds/glycolysis.ogg"))
		self.etc_buzz = arcade.load_sound(resource_path("assets/sounds/etc.ogg"))

		arcade.set_background_color(arcade.csscolor.CORNFLOWER_BLUE)

	def setup(self, level):
		self.view_bottom = 0
		self.view_left = 0

		self.player_list = arcade.SpriteList()
		self.wall_list = arcade.SpriteList()
		self.atp_list = arcade.SpriteList()
		self.glucose_list = arcade.SpriteList()
		self.o2_list = arcade.SpriteList()
		self.background_list = arcade.SpriteList()
		self.lever_list = arcade.SpriteList()
		self.glycolysis_list = arcade.SpriteList()

		self.player = PlayerCharacter()
		self.player.update_animation()
		self.player.center_x = 64
		self.player.center_y = 128
		self.player_list.append(self.player)

		# --- Load Map ---
		map_path = resource_path(f"assets/levels/level_{level}.tmx")
		platforms_layer_name = "Platforms"
		atp_layer_name = "ATP"
		glucose_layer_name = "Glucose"
		background_layer_name = "Background"
		flag_layer_name = "NextLevel"
		o2_layer_name = "O2"
		lever_layer_name = "Levers"
		glycolysis_layer_name = "GlycolysisMachine"
		conveyer_belt_layer_name = "ConveyerBelts"
		try:
			current_map = arcade.tilemap.read_tmx(map_path)
		except FileNotFoundError:
			self.level = 1
			self.setup(self.level)
			self.view_left = 0
			self.view_bottom = 0
			changed = True
			return

		self.end_of_map = current_map.map_size.width * GRID_PIXEL_SIZE
		self.wall_list = arcade.tilemap.process_layer(map_object=current_map, layer_name=platforms_layer_name, scaling=TILE_SCALING, use_spatial_hash=True)
		self.atp_list = arcade.tilemap.process_layer(current_map, atp_layer_name, COIN_SCALING)
		self.o2_list = arcade.tilemap.process_layer(current_map, o2_layer_name, COIN_SCALING)
		self.background_list = arcade.tilemap.process_layer(current_map, background_layer_name, TILE_SCALING)
		self.flag_list = arcade.tilemap.process_layer(current_map, flag_layer_name, TILE_SCALING)
		self.glucose_list = arcade.tilemap.process_layer(current_map, glucose_layer_name, COIN_SCALING)
		self.lever_list = arcade.tilemap.process_layer(current_map, lever_layer_name, COIN_SCALING)
		self.glycolysis_list = arcade.tilemap.process_layer(current_map, glycolysis_layer_name, TILE_SCALING)
		self.conveyer_belt_list = arcade.tilemap.process_layer(current_map, conveyer_belt_layer_name, TILE_SCALING)
		for sprite in self.conveyer_belt_list:
			self.wall_list.append(sprite)

		for lever in self.lever_list:
			lever.onoff = 0
			lever.stepoff = 0
			lever.cycle = 0
		for glycolysis in self.glycolysis_list:
			glycolysis.stepoff = 0

		if current_map.background_color:
			arcade.set_background_color(current_map.background_color)

		self.physics_engine = arcade.PhysicsEnginePlatformer(self.player, self.wall_list, GRAVITY)

	def on_draw(self):
		arcade.start_render()

		self.wall_list.draw()
		self.background_list.draw()
		self.wall_list.draw()
		self.atp_list.draw()
		self.flag_list.draw()
		self.glucose_list.draw()
		self.o2_list.draw()
		self.lever_list.draw()
		self.glycolysis_list.draw()
		self.conveyer_belt_list.draw()

		if self.level == 1:
			arcade.draw_text("Welcome to the MitoConn research facility,\nand I hope you enjoy your tour. Press A and D\nor the left and right arrows to move left and right", 0, 275, arcade.color.BLACK, 14)
			arcade.draw_text("Press W, up arrow, or space to jump", 525, 275, arcade.color.BLACK, 14)
			arcade.draw_text("Collect things like ATP, O2, and Glucose", 1050, 275, arcade.color.BLACK, 14)
			arcade.draw_text("Walk onto the flag\nto go to the next level", 1450, 350, arcade.color.BLACK, 14)
		elif self.level == 2:
			arcade.draw_text("Flip the lever to turn on the oxygen compressor,\nthen wait to accumulate six O2", 400, SCREEN_HEIGHT - 100, arcade.color.BLACK, 14)
		elif self.level == 3:
			if self.trigger1 == 0:
				arcade.draw_text("This is the glycolysis chamber. We\'ve already supplied the glycolysis machine\nwith some of the required inredients, 2 NAD+ and 2 ADP. We just need you to\nsupply the glucose. Just walk up to the chamber and put it in.", 0, 275, arcade.color.BLACK, 14)
			else:
				arcade.draw_text("The glycolysis chamber seems to have output something called a pyruvate.\nThese 2 pyruvates will be used later on, so I\'ll hold on to them for now.\nIt also turned the 2 ADP into 2 ATP and the 2 NAD+ into 2 NADH, which I\'ll also keep.\nHead on over to the flag and I\'ll see you in the MitoConn MaTRiX wing.", 0, 275, arcade.color.BLACK, 14)
		elif self.level == 4:
			arcade.draw_text("Welcome to the pyruvate oxidation chamber.\nI\'ve already supplied the CoA-SH, NAD+, and\nthe pyruvates from the last chamber,\nyou just need to turn on the machine.", 0, 275, arcade.color.BLACK, 14)
			for lever in self.lever_list:
				if lever.cycle == 1:
					arcade.draw_text("Good, the machine has taken the molecules and turned the CoA-SH into CO2,\nthe NAD+ into NADH, and the pyruvates into Acetyl CoA.\nThis will be useful for the Kreb room", 300, (650*2) - 100, arcade.color.BLACK, 14)
					break
		elif self.level == 5:
			arcade.draw_text("Welcome to the Kreb room. This is a dangerous room as there is a lot of stuff going on,\nmind your step, the machine toggle should be up there somewhere", 0, 275, arcade.color.BLACK, 14)
			for lever in self.lever_list:
				if lever.cycle == 1:
					arcade.draw_text("Good, the Kreb Machine™ should be up and running now. It looks like it\'s made\nsome NADH, some FADH2, as well as some CO2. I\'ll get rid of the CO2,\nand keep the other stuff. If you find any extra stuff lying around you can take it.", (800), (SCREEN_HEIGHT * 2) - 250, arcade.color.BLACK, 14)
					break
				else:
					arcade.draw_text("Here\'s the toggle for the Kreb Machine™, I\'ve taken some materials from previous chambers\nas well as some other stuff, like FAD. We also have some exposed ADP and NAD+,\nso be careful around that. It uses our SLP technology to transfer the phosphate group\nfrom the glucose to the ADP", (800), (SCREEN_HEIGHT * 2) - 250, arcade.color.BLACK, 14)
					break
		elif self.level == 6:
			arcade.draw_text("Welcome to the final stage of the respiration\nprocess. We have brought the required\nelectrons here, the ones from the glycolysis\nchamber were brought by the NADH\ntube, and the ones from the\nKreb Machine™ were brought here\nvia the NADH and FADH2 tubes", -200, 400, arcade.color.BLACK, 14)
			arcade.draw_text("Be careful around this machine, there\'s a lot of hydrogen ions contained within it,\nand once you flip that lever, the electrons will move across the membrane via chemiosmosis\nand it will open the valves for the NADH, the FADH2,\nand the ADP + Pi, and a large amount of energy will be released. This energy\nwill be captured with the MitoConn Energy Bond Apparatus, or the MEBA.\nThe MEBA utilizes chemical bonds to store the energy produced, so it\'s very reliable", (500), (SCREEN_HEIGHT * 2) - 300, arcade.color.BLACK, 14)
		elif self.level == 7:
			arcade.draw_text("You might be wondering what happens if we don\'t have oxygen.\nWhen this happens, we use something called fermentation. We\nsupply the machine with glucose and it produces NAD+, ATP, CO2,\nand Ethanol. Why don\'t you give it a try?", 0, 275, arcade.color.BLACK, 14)
			for lever in self.lever_list:
				if lever.cycle == 1:
					arcade.draw_text("Good, now we have some extra ATP,\nas well as some CO2, NAD+, and ethanol", 0, 700, arcade.color.BLACK, 14)
		elif self.level == 8:
			arcade.draw_text("Attributions", 0, 275, arcade.color.BLACK, 14)
			arcade.draw_text("engine.wav, engine_continue.wav, engine_stop.wav:\n\"Old Car Starting Sound\"\n(https://soundbible.com/2214-Old-Car-Starting.html)\nby Daniel Simion is licensed under\nCC BY-ND 3.0 (https://creativecommons.org/licenses/by/3.0/)", 200, 275, arcade.color.BLACK, 14)
			arcade.draw_text("icon - Lisawerner9, CC BY-SA 4.0\n<https://creativecommons.org/licenses/by-sa/4.0>,\nvia Wikimedia Commons", 750, 275, arcade.color.BLACK, 14)
			arcade.draw_text("Sprites - (https://kenney.nl) licensed under\nCC BY 1.0 (https://creativecommons.org/licenses/by/1.0/)", 1200, 275, arcade.color.BLACK, 14)
		arcade.draw_text("ATP: {}".format(str(self.atp)), 10 + self.view_left, self.view_bottom + SCREEN_HEIGHT - 30, arcade.color.BLACK, 14)
		arcade.draw_text("O2: {}".format(str(self.o2)), 10 + self.view_left, self.view_bottom + SCREEN_HEIGHT - 50, arcade.color.BLACK, 14)
		arcade.draw_text("Glucose: {}".format(str(self.glucose)), 10 + self.view_left, self.view_bottom + SCREEN_HEIGHT - 70, arcade.color.BLACK, 14)

		self.player_list.draw()

	def on_key_press(self, key, modifiers):
		if key == arcade.key.UP or key == arcade.key.W or key == arcade.key.SPACE:
			if self.physics_engine.can_jump():
				self.player.change_y = PLAYER_JUMP_SPEED
				arcade.play_sound(self.jump_sound)
		elif key == arcade.key.DOWN or key == arcade.key.S:
			self.player.change_y = -PLAYER_MOVEMENT_SPEED
		elif key == arcade.key.LEFT or key == arcade.key.A:
			self.player.change_x = -PLAYER_MOVEMENT_SPEED
		elif key == arcade.key.RIGHT or key == arcade.key.D:
			self.player.change_x = PLAYER_MOVEMENT_SPEED

	def on_key_release(self, key, modifiers):
		if key == arcade.key.UP or key == arcade.key.W or key == arcade.key.SPACE:
			self.player.change_y = 0
		elif key == arcade.key.DOWN or key == arcade.key.S:
			self.player.change_y = 0
		elif key == arcade.key.LEFT or key == arcade.key.A:
			self.player.change_x = 0
		elif key == arcade.key.RIGHT or key == arcade.key.D:
			self.player.change_x = 0

	def on_update(self, delta_time):

		self.tick += 1
		if self.tick > 60:
			self.tick = 0

		# Move the player with the physics engine
		self.physics_engine.update()

		self.player.update_animation()

		if self.glucose < 0:
			self.glucose = 0
		if self.atp < 0:
			self.atp = 0
		if self.o2 < 0:
			self.o2 = 0

		conveyer_belt_hit_list = []
		for conveyer in self.conveyer_belt_list:
			conveyer_belt_hit_list.append(conveyer.position[0])
		if len(conveyer_belt_hit_list) > 0:
			if self.player.position[0] > conveyer_belt_hit_list[0] and self.player.position[0] < conveyer_belt_hit_list[-1]:
				self.onconveyer = 1
				self.player.change_x = PLAYER_MOVEMENT_SPEED / 2
			else:
				if self.onconveyer == 1:
					self.player.change_x = 0
					self.onconveyer = 0

		# Collectable collisions
		atp_hit_list = arcade.check_for_collision_with_list(self.player, self.atp_list)
		for atp in atp_hit_list:
			atp.remove_from_sprite_lists()
			arcade.play_sound(self.collect_coin_sound)
			if self.level == 6:
				self.atp += 34
			else:
				self.atp += 1

		glucose_hit_list = arcade.check_for_collision_with_list(self.player, self.glucose_list)
		for glucose in glucose_hit_list:
			glucose.remove_from_sprite_lists()
			arcade.play_sound(self.collect_coin_sound)
			self.glucose += 1

		o2_hit_list = arcade.check_for_collision_with_list(self.player, self.o2_list)
		for o2 in o2_hit_list:
			o2.remove_from_sprite_lists()
			arcade.play_sound(self.collect_coin_sound)
			self.o2 += 1

		glycolysis_hit_list = arcade.check_for_collision_with_list(self.player, self.glycolysis_list)
		for glycolysis in glycolysis_hit_list:
			if glycolysis.stepoff == 0 and self.glucose >= 1:
				if self.audioplayer != None:
					if self.glycolysis_sound.is_playing(self.audioplayer):
						arcade.stop_sound(self.audioplayer)
						self.audioplayer = None
				self.audioplayer = arcade.play_sound(self.glycolysis_sound)

				self.glucose -= 1
				self.atp += 2
				if self.level == 3:
					self.trigger1 = 1

				glycolysis.stepoff = 1
		for glycolysis in [glycolysis for glycolysis in self.glycolysis_list if glycolysis not in glycolysis_hit_list]:
			glycolysis.stepoff = 0

		# Fallen off map
		if self.player.center_y < -100:
			self.player.center_x = PLAYER_START_X
			self.player.center_y = PLAYER_START_Y

			self.view_left = 0
			self.view_bottom = 0
			changed = True
			arcade.play_sound(self.game_over)

		# Flag collision detection
		flag_hit_list = arcade.check_for_collision_with_list(self.player, self.flag_list)
		if len(flag_hit_list) > 0:
			if self.level == 1:
				if self.glucose < 1:
					return
			elif self.level == 2:
				if self.o2 < 6:
					return
				if self.audioplayer != None:
					if self.engine_continue.is_playing(self.audioplayer):
						arcade.stop_sound(self.audioplayer)
					elif self.engine_start.is_playing(self.audioplayer):
						arcade.stop_sound(self.audioplayer)
			self.level += 1
			self.setup(self.level)
			self.view_left = 0
			self.view_bottom = 0
			changed = True

		# Lever toggling
		levertextures = [
			arcade.load_texture(resource_path("assets/images/tiles/leverLeft.png")),
			arcade.load_texture(resource_path("assets/images/tiles/leverRight.png"))
		]
		lever_hit_list = arcade.check_for_collision_with_list(self.player, self.lever_list)
		for lever in lever_hit_list:
			if lever.stepoff == 0:
				lever.stepoff = 1
				arcade.play_sound(self.lever_sound)
				if lever.onoff == 1:
					lever.onoff = 0
					lever.cycle = 1
				else:
					lever.onoff = 1
					lever.cycle = 1
				lever.texture = levertextures[lever.onoff]

		for lever in [lever for lever in self.lever_list if lever not in lever_hit_list]:
			lever.stepoff = 0

		for lever in self.lever_list:
			# On and off engine noises
			if self.level == 2:
				if lever.onoff == 1:
					if self.tick == 30:
						if self.o2 < 6:
							self.o2 += 1

					if self.audioplayer == None:
						time.sleep(0.5)
						self.audioplayer = arcade.play_sound(self.engine_start)

					elif self.engine_start.get_stream_position(self.audioplayer) + 1 >= self.engine_start.get_length() or self.engine_continue.get_stream_position(self.audioplayer) + 1 >= self.engine_continue.get_length():
						self.audioplayer = arcade.play_sound(self.engine_continue)
					lever.cycle = 0
				elif lever.cycle == 1:
					lever.cycle = 0
					if self.engine_continue.is_playing(self.audioplayer) or self.engine_start.is_playing(self.audioplayer):
						arcade.play_sound(self.engine_stop)
						if self.engine_continue.is_playing(self.audioplayer):
							arcade.stop_sound(self.audioplayer)
						elif self.engine_start.is_playing(self.audioplayer):
							arcade.stop_sound(self.audioplayer)
						self.audioplayer = None
			elif self.level == 6:
				if lever.onoff == 1 and self.o2 - 6 >= 0:
					time.sleep(0.5)
					self.audioplayer = arcade.play_sound(self.etc_buzz)
					lever.cycle = 0
					self.o2 -= 6
				elif lever.cycle == 1:
					lever.cycle = 0
					if self.audioplayer != None:
						if self.etc_buzz.is_playing(self.audioplayer):
							arcade.stop_sound(self.audioplayer)
							self.audioplayer = None
			elif self.level == 7:
				if lever.onoff == 1 and self.glucose >= 1:
					self.glucose -= 1

		# --- Manage Scrolling ---

		# Track if viewport needs to be changed
		changed = False

		# left scrolling
		left_boundary = self.view_left + LEFT_VIEWPORT_MARGIN
		if self.player.left < left_boundary:
			self.view_left -= left_boundary - self.player.left
			changed = True

		# right scrolling
		right_boundary = self.view_left + SCREEN_WIDTH - RIGHT_VIEWPORT_MARGIN
		if self.player.right > right_boundary:
			self.view_left += self.player.right - right_boundary
			changed = True

		# Scroll up
		top_boundary = self.view_bottom + SCREEN_HEIGHT - TOP_VIEWPORT_MARGIN
		if self.player.top > top_boundary:
			self.view_bottom += self.player.top - top_boundary
			changed = True

		# Scroll down
		bottom_boundary = self.view_bottom + BOTTOM_VIEWPORT_MARGIN
		if self.player.bottom < bottom_boundary:
			self.view_bottom -= bottom_boundary - self.player.bottom
			changed = True

		if changed:
			self.view_bottom = int(self.view_bottom)
			self.view_left = int(self.view_left)
			arcade.set_viewport(self.view_left, SCREEN_WIDTH + self.view_left, self.view_bottom, SCREEN_HEIGHT + self.view_bottom)

def main():
	window = BioGame()
	window.setup(window.level)
	arcade.run()

if __name__ == "__main__":
	main()
