# -*- mode: python ; coding: utf-8 -*-

import os
import shutil
import xml.etree.ElementTree as ET

block_cipher = None


a = Analysis(['biogame.py'],
             pathex=['./'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

if os.path.exists("./tmplevels"):
	shutil.rmtree("./tmplevels")
shutil.copytree("assets/levels", "./tmplevels")
for file in os.listdir("assets/levels"):
	tree = ET.parse(os.path.join("assets/levels/", file))
	for tile in tree.getroot()[0].findall("tile"):
		tile[0].attrib["source"] = os.path.split(tile[0].attrib["source"])[1]
	tree.write(os.path.join("assets/levels/", file))
for root, dirs, files in os.walk("assets"):
	for file in files:
		a.datas += [(os.path.split(file)[1], os.path.join(root, file), 'Data')]

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='biogame',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False )
for root, dirs, files in os.walk("assets"):
	for file in files:
		a.datas += [(os.path.split(file)[1], os.path.join(root, file), 'Data')]

shutil.rmtree("./assets/levels")
shutil.copytree("./tmplevels", "./assets/levels")
shutil.rmtree("./tmplevels")